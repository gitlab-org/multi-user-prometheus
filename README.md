Multi-user Prometheus is a project to allows you to use one Prometheus instance with multiple users.

## Why

- We want to people using GitLab to be able to have the functionality of multi-user Prometheus.
- We want the people that need to use Prometheus with multiple users to consider installing GitLab.

## Functionality

Consider [https://github.com/bitly/oauth2_proxy](https://github.com/bitly/oauth2_proxy) as the basis of it since it already has OAuth and https support.

1. Add OAuth support
1. Maybe easy setup of https? [Google Doc that is internal to GitLab team members](https://docs.google.com/document/d/1ZXYfQH75CbzgLGQXJtIPn-2UsjHb0bICMj4Ibqc9bsQ/edit)
1. Query proxy that changes queries to be relevant for one user (you can only see the metrics of the projects that you are a team member of in GitLab)
1. Make it work with auto deploy
1. Isolation of resource usage
1. Audit trail
1. Role based access control

This is not multi-tenant (multiple organization) Prometheus, Weaveworks is working on that.

Improbable runs it with a tenant label, expose only the query side to certain customers.

We assume one organization using one GitLab instance.
We need a user label for the scrape configuration.
Multi user and multi tenant is the same thing.
Read side is easy to build.
Write side is harder.
Prometheus can add a tenant label for a certain project.
We could do it like this in the GitLab Prometheus design if we use Omnibus.
We need a label matching proxy server.
Worth prototyping the read path.

## Packaging

People can use the Go binary made based on this repo. But we'll also have it as part of our omnibus packages. We install Prometheus already. Now will put Multi-user Prometheus in front. Set up with GitLab OAuth like Mattermost.

## Servers

Currently we have one Prometheus server per GitLab installation.
We should continue to use this server for GitLab metrics, federated metrics from other servers, and alerting.
But every application deployed with GitLab should get its own Prometheus pod to monitor that application in the future.
The main Prometheus server can get metrics from the application specific servers to help with alerting.

## Structure

1. Nginx in front (not sure if this is needed, ask Jacob Vosmaer how we did this with workhorse, may be simpler to skip it)
1. Multi-user Prometheus (this repo, Go application)
1. Prometheus (that already ships with GitLab)

## When

Shipping the rest of Prometheus has priority. Julius gets time for this because https/OAuth support is a priority.

## People

See the [GitLab team page](https://about.gitlab.com/team/).

- Maintainer: Julius Volz
- Lead: Ben Kochie
- Product manager: Joshua Lambert
- Packaging: Marin Jankovski

## License

This project has a [proprietary license](https://gitlab.com/gitlab-org/multi-user-prometheus/blob/master/LICENSE), but the code and issues are publicly viewable and the current version is free to use.
